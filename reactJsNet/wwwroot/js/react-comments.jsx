﻿class CommentBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = { data: [] };
        this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
    }

    componentWillMount() {
        this.getData();
        window.setInterval(() =>
            this.getData(),
            this.props.pollInterval,
        );
    }

    getData() {
        const xhr = new XMLHttpRequest();
        xhr.open('get', this.props.url, true);
        xhr.onload = () => {
            console.log(xhr.responseText);
            const data = JSON.parse(xhr.responseText);
            this.setState({ data: data });
        };
        xhr.send();
    }

    handleCommentSubmit(comment) {
        // TODO: submit to the server and refresh the list
        console.log(comment);
        const data = new FormData();
        data.append('author', comment.Author);
        data.append('text', comment.Text);

        const xhr = new XMLHttpRequest();
        xhr.open('post', this.props.submitUrl, true);
        xhr.onload = () => this.getData();
        xhr.send(data);
    }

    render() {
        return (
            <div className="commentBox">
                <h1>Comments</h1>
                <hr />
                <CommentList data={this.state.data} />
                <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
            </div>
        );
    }
}

//render() { return <span>{JSON.stringify(this.props)}</span> }
class CommentList extends React.Component {
    render() {
        var data = this.props.data.map(function (comment) {
            return (
                <Comment author={comment.author} key={comment.id}>
                    {comment.text}
                </Comment>
            );
        });
        return <div className="CommentList">{data}</div>;
    }
}


class Comment extends React.Component {
    render() {
        return (
            <div className="comment p-2 mb-2 border">
                <h2 className="commentAuthor">{this.props.author}</h2>
                {this.props.children}
            </div>
        );
    }
}

class CommentForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = { author: '', text: '' };
        this.handleAuthorChange = this.handleAuthorChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleAuthorChange(e) {
        this.setState({ author: e.target.value });
        //console.log(this.state.author);
    }

    handleTextChange(e) {
        this.setState({ text: e.target.value });
        //console.log(this.state.text);
    }

    handleSubmit(e) {
        e.preventDefault();
        console.log(e.target);
        const author = this.state.author.trim();
        const text = this.state.text.trim();
        if (!text || !author) {
            return;
        }
        // TODO: send request to the server
        this.props.onCommentSubmit({ Author: author, Text: text });
         //set state
        this.setState({ author: '', text: '' });
    }

    render() {
        return (
            <form className="commentForm mt-4" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Your name" value={this.state.author} onChange={this.handleAuthorChange} />
                <input type="text" placeholder="Say something..." value={this.state.text} onChange={this.handleTextChange} />
                <input type="submit" value="Post" />
            </form>
        );
    }
}



ReactDOM.render(<CommentBox url="/comments" submitUrl="/comments/new" pollInterval={4000} />, document.getElementById('content'));