﻿class CommentList extends React.Component {
    render() {
        return (
            <div className="commentList">
                <h2 className="commentAuthor">{this.props.name}</h2>
            </div>
        );
    }
}

class CommentForm extends React.Component {
    render() {
        return (
            <div className="commentForm">Hello, world! I am a CommentForm.</div>
        );
    }
}

class CommentBox extends React.Component {
    render() {
        return (
            <div>
                <div className="commentBox">Hello, world! I am a CommentBox.</div>
                <CommentList name="Pete Hunt"></CommentList>
                <CommentForm name="Pete Hunt2"></CommentForm>
            </div>
        );
    }
}

ReactDOM.render(<CommentBox />, document.getElementById('content'));