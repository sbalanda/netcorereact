# tutorial

- https://reactjs.net/tutorials/aspnetcore.html

# netCoreReact

- install package React.AspNet
- JavaScriptEngineSwitcher.ChakraCore 
- JavaScriptEngineSwitcher.Extensions.MsDependencyInjection
- JavaScriptEngineSwitcher.ChakraCore.Native.win-x64  (creo que no es necesario)

# otros

- las llamadas a los js tienten que tener type="text/jsx"
        
        <script type="text/jsx" src="@Url.Content("~/js/react-comments.jsx")">
        
- en mi caso sin babel no funcionaba 

        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.js"></script>

